// ソースコード1 複数波形の同時生成
#include <Waon.h>

void setup()
{
  // D3 ~ D5 ピンを演奏用に予約 (0000 0000 0011 1000)
  Waon::setup(0x0038); //Hex
}

void loop()
{
  Waon::tone(3,261); // ド
  Waon::tone(4,329); // ミ  
  Waon::tone(5,392); // ソ
  delay(2000);
  Waon::tone(3,0);   // 止める
  Waon::tone(4,0);   // 止める
  Waon::tone(5,0);   // 止める
  delay(2000);
}
