// ソースコード3 じわじわ来るサンプル
#include <Waon.h>

void setup()
{
  // D3 ~ D5 ピンを演奏用に予約 (0000 0000 0011 1000)
  Waon::setup(0x0038); //Hex
}

void loop()
{
  int r,g,b;
  for(g = 0; g <= 100; g++){
      for(b = 0; b <= 100; b++){
	  for(r = 0; r <= 100; r++){
	    Waon::tone(3, 261, g); // D3 ピンが緑に接続
	    Waon::tone(4, 329, b); // D4 ピンが青に接続
	    Waon::tone(5, 392, r); // D5 ピンが赤に接続
	    delay(10);
	  }
      }
  }
  Waon::tone(3,0);   // 止める
  Waon::tone(4,0);   // 止める
  Waon::tone(5,0);   // 止める
  delay(2000);
}
