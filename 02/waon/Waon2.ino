// ソースコード2 デューティと音質の変化
#include <Waon.h>

void setup()
{
  // D3 ~ D5 ピンを演奏用に予約 (0000 0000 0011 1000)
  Waon::setup(0x0038); //Hex
}

void loop()
{
  Waon::tone(3,261,75); // ド (デューティ 75%)
  Waon::tone(4,329);    // ミ  
  Waon::tone(5,392,10); // ソ (デューティ 10%)
  delay(2000);
  Waon::tone(3,0);   // 止める
  Waon::tone(4,0);   // 止める
  Waon::tone(5,0);   // 止める
  delay(2000);
}
