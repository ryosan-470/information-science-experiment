// 実験課題1.3
// 簡単な信号機の制作
int GPin = 10; // 緑
int YPin = 11; // 黃
nt RPin = 12; // 赤

void setup()
{
  // 各ピンを出力に使用
  pinMode(GPin, OUTPUT);
  pinMode(YPin, OUTPUT);
  pinMode(RPin, OUTPUT);
}

void loop()
{
  digitalWrite(GPin, HIGH);
  delay(3000);
  digitalWrite(GPin, LOW);

  digitalWrite(YPin, HIGH);
  delay(1000);
  digitalWrite(YPin, LOW);

  digitalWrite(RPin, HIGH);
  delay(2000);
  digitalWrite(RPin, LOW);
}
