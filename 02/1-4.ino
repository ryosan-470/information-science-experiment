// 実験課題1.4
// 2つのLEDを別々の周期で点灯させる
int ledG = 10;
int ledR = 12;
int G_state; // Green LEDの点灯状態を表す変数
int R_state; // Red LEDの点灯状態を表す変数
int G_length = 1; // Green LEDの点灯(消灯)の長さとして最初にセットする値
int R_length = 2;// Red LEDの点灯(消灯)の長さとして最初にセットする値
int G_count; // Green LED に関する時間経過を表す変数
int R_count; // Red LED に関する時間経過を表す変数

void setup ()
{  
  pinMode(ledG ,OUTPUT);
  pinMode(ledR, OUTPUT);
  G_state = HIGH;
  R_state = HIGH;
  G_count = 0;
  R_count = 0;
}

void loop ()
{
  if(check_time(G_count, G_length) == 1){
    changeLED(ledG, G_state);    
  }
  G_count++;

  delay(250);
}

// 時間の経過をチェックするタイマ機能の関数
int check_time (int led_count, int led_length) {
  if(led_count == led_length -1) return 1;
    else return 0;
}
// LEDの状態を変える関数
void changeLED(int pin, int state){
  if(state == LOW) {
    digitalWrite(pin, HIGH);
    state = HIGH;
  }
  else{    
    digitalWrite(pin, LOW);
    state = LOW;
  }
}
