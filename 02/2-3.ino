// 実験課題2-2
// 太陽光の入力により明るさを変化させる
int pin0 = 12;
int pin1 = 11;
int pin2 = 10;
int pin3  = 9;
int value = 0;
// アナログINピン0番に接続
int sensorPin = 0;
void setup(){
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);

  digitalWrite(pin0, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);        
  
  Serial.begin(9600);
}

void loop(){
  value = analogRead(sensorPin);
  if(value > 255)
    value = 255;

  if(value >= 0 && value < 50){
    digitalWrite(pin0, HIGH);
    digitalWrite(pin1, LOW);
    digitalWrite(pin2, LOW);
    digitalWrite(pin3, LOW);        
  }else if(value >= 50 && value < 100){
    digitalWrite(pin0, HIGH);
    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, LOW);
    digitalWrite(pin3, LOW);       
  }else if(value >= 100 && value < 150){
    digitalWrite(pin0, HIGH);
    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, HIGH);
    digitalWrite(pin3, LOW);      
  }else{
    digitalWrite(pin0, HIGH);
    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, HIGH);
    digitalWrite(pin3, HIGH);  
  }      
  Serial.println(value);
}
