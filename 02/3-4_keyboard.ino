// 実験課題3-4
// タクトスイッチを使った簡易キーボードの制作
int Do = 956;
int Re = 851;
int Mi = 758;
int Fa = 716;
int So = 638;
int Ra = 568;
int Si = 506;
int DoU = 478;
int ReU = 426;

int B16 = 125;
int B8 = B16 * 2;
int B4 = B16 * 4;
int B2 = B16 * 8;

int speaker = 12;

int Do_SWH = 10;
int Re_SWH = 9;
int Mi_SWH = 8;
int Fa_SWH = 7;
int So_SWH = 6;
int Ra_SWH = 5;
int Si_SWH = 4;
int DoU_SWH = 3;

void sound(int tone_delay){
  digitalWrite(speaker, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(speaker, LOW);
  delayMicroseconds(tone_delay);
}

void setup(){
  pinMode(speaker, OUTPUT);
  pinMode(Do_SWH, INPUT_PULLUP);
  pinMode(Re_SWH, INPUT_PULLUP);
  pinMode(Mi_SWH, INPUT_PULLUP);
  pinMode(Fa_SWH, INPUT_PULLUP);
  pinMode(So_SWH, INPUT_PULLUP); 
  pinMode(Ra_SWH, INPUT_PULLUP);
  pinMode(Si_SWH, INPUT_PULLUP);
  pinMode(DoU_SWH, INPUT_PULLUP);
}

void loop(){
  if(digitalRead(Do_SWH) == LOW){
    sound(Do);
  }else if(digitalRead(Re_SWH) == LOW){
    sound(Re);
  }else if(digitalRead(Mi_SWH) == LOW){
    sound(Mi);
  }else if(digitalRead(Fa_SWH) == LOW){
    sound(Fa);
  }else if(digitalRead(So_SWH) == LOW){
    sound(So);
  }else if(digitalRead(Ra_SWH) == LOW){
    sound(Ra);
  }else if(digitalRead(Si_SWH) == LOW){
    sound(Si);
  }else if(digitalRead(DoU_SWH) == LOW){
    sound(DoU);
  }
}
