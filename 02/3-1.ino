// 実験課題3-1
// ある周波数の音を再生する
int low = 400;
int high = 800;
int B16 = 125;
int B8 = B16 * 2;
int B4 = B16 * 4;
int B2 = B16 * 8;
// デジタル12番ピンにスピーカー接続
int speaker = 12;

void sound(int tone_delay){
  digitalWrite(speaker, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(speaker, LOW);
  delayMicroseconds(tone_delay);
}

void play(int tone, int length){
  unsigned long tone_end_time;
  tone_end_time = millis() + length;

  while(millis() < tone_end_time){
    sound(tone);
  }
}

void yasumi(int length){
  delay(length);
}

void setup(){
  pinMode(speaker, OUTPUT);
}

void loop(){
  play(low, B4);
  play(high, B4);

  yasumi(2000);
}
