// 実験課題 1-1
// タクトスイッチによるLED点滅

int SWITCH = 13; // スイッチの入力をデジタル13番ピンに接続
int ledPin = 12; // デジタル12番ピンにLEDを接続

void setup()
{
  pinMode(SWITCH, INPUT);  // SWITCH(13番)ピンを入力モードに
  pinMode(ledPin, OUTPUT); // ledPin(12番)ピンを出力に使用
}

void loop()
{
  // タクトスイッチが押されたか 押されるとHIGHになる
  if(digitalRead(SWITCH) == HIGH)
    digitalWrite(ledPin, HIGH);  // LED点灯
  else
    digitalWrite(ledPin, LOW);   // LED消灯
}
