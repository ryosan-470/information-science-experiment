// 太陽電池からの入力値測定
int sensVal;
// アナログINピン0番に接続
int sensorPin = 0;

void setup(){
  // シリアル通信のデータ転送レート(bps)
  Serial.begin(9600);
}

void loop(){
  // sensorPin 番の値を読み取る
  sensVal = analogRead(sensorPin);
  // sensVal の値をシリアルモニタに表示
  Serial.println(sensVal);
  delay(1000);
}
