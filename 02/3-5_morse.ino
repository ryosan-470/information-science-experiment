// 実験課題3
// モールス信号とLEDを組み合わせ
int low = 1500;
int To = 80;     // ト
int Tu = To * 3; // ツー トの音の3倍
// デジタル13番ピンにスピーカー接続
int speaker = 13;
// デジタルA番からLED
int A = 4;
// スイッチ
int To_SWH = 3;
int Tu_SWH = 2;

int to_now;
int tu_now;
int to_last;
int tu_last;

long lastDebounceTime = 0; 
long debounceDelay = 50; 

void sound(int tone_delay){
  digitalWrite(speaker, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(speaker, LOW);
  delayMicroseconds(tone_delay);
}

void play(int tone, int length){
  unsigned long tone_end_time;
  tone_end_time = millis() + length;

  while(millis() < tone_end_time){
    sound(tone);
  }
}

void setup(){
  int ledPin;
  pinMode(speaker, OUTPUT);
  pinMode(To_SWH, INPUT_PULLUP);
  pinMode(Tu_SWH, INPUT_PULLUP);
  for(ledPin = 4; ledPin < 12; ledPin++){
    pinMode(ledPin, OUTPUT);
  }
}

void loop(){
  to_now = digitalRead(To_SWH); 
  if(to_last == LOW && to_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
      // 半分(3つ)点灯
      lump(8);
      play(low, To);
    }
    lastDebounceTime = now;
  }
  to_last = to_now; 

  tu_now = digitalRead(Tu_SWH); 
  if(tu_last == LOW && tu_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
      // 全部点灯
      lump(12);
      play(low, Tu);
    }
    lastDebounceTime = now;
  }
  tu_last = tu_now; 
}
// A番目からj番目まで点灯しそのあと消灯
void lump(int j){
  int i;
  for(i = A; i < j; i++){
    digitalWrite(i, HIGH);
  }
  delay(30);

  for(i = A; i < j; i++){
    digitalWrite(i, LOW);
  }
  delay(5);
}
