// 実験課題2-2
// 太陽光の入力により明るさを変化させる
int ledPin = 11;
int value = 0;
// アナログINピン0番に接続
int sensorPin = 0;
void setup(){
  pinMode(ledPin, LOW);
  Serial.begin(9600);
}

void loop(){
  value = analogRead(sensorPin);
  if(value > 255)
    value = 255;
  Serial.println(value);
  analogWrite(ledPin, value);
}
