// 実験課題1.5
// 信号機の点灯を1つのスイッチで切り替える
int SWITCH = 13;
int GPin = 10; // 緑
int YPin = 11; // 黃
int RPin = 12; // 赤

int G_st;  
int Y_st;
int R_st;
int SW_now; // 現在のスイッチの状態 
int SW_last;  // 前回のスイッチの状態
long lastDebounceTime = 0; 
long debounceDelay = 50; 


void setup ()
{  
  pinMode(SWITCH , INPUT);
  pinMode(GPin, OUTPUT);
  pinMode(YPin, OUTPUT);
  pinMode(RPin, OUTPUT);
  G_st = LOW;
  Y_st = LOW;
  R_st = HIGH;
  digitalWrite(RPin, HIGH);
}

void loop () {
  SW_now = digitalRead(SWITCH); 
  if(SW_last == LOW && SW_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
      changeState();
      changeColor();
    }
    lastDebounceTime = now;
  }
  SW_last = SW_now; 
}

void changeState(){
  if(R_st == HIGH){
    R_st = LOW;
    G_st = HIGH;
  }
  else if(Y_st == HIGH){
    Y_st = LOW;
    R_st = HIGH;
  }
  else if(G_st == HIGH){
    G_st = LOW;
    Y_st = HIGH;
  }
}
void changeColor(){
  if(R_st == HIGH)
    digitalWrite(RPin, HIGH);
  else
    digitalWrite(RPin, LOW);

  if(Y_st == HIGH)
    digitalWrite(YPin, HIGH);
  else
    digitalWrite(YPin, LOW);

  if(G_st == HIGH)
    digitalWrite(GPin, HIGH);
  else
    digitalWrite(GPin, LOW);
}
