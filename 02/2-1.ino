// 実験課題2-1
// LEDの明るさをある時間間隔で変化させる
int ledPin = 11;
int value = 0;
boolean flag = true;
void setup(){
  pinMode(ledPin, LOW);
}

void loop(){
  // flag == true なら 登る
  if(flag){
    analogWrite(ledPin, value);
    delay(1000);    
    value += 31;
    if(value > 245){
      value = 255;
      flag = false;
    }
  }
  else{
    analogWrite(ledPin, value);
    delay(1000);    
    value -= 31;
    if(value <= 0){
      value = 0;
      flag = true;
    }
      
  }
}
