// ソースコード2 タイマ割り込みを用いたスケッチの例
#include <TimerOne.h>
volatile boolean state = false;
void setup(){
  pinMode(12, OUTPUT);
  pinMode(2, OUTPUT);

  Timer1.initialize();
  Timer1.attachInterrupt(wakeup, 500000);
}

void loop(){
  digitalWrite(12, HIGH);
  delay(50);
  digitalWrite(12, LOW);
  delay(50);
}

void wakeup(){
  state = !state;
  if(state) digitalWrite(2. HIGH);
  else digitalWrite(2, LOW);
}
