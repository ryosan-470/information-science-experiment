## 課題提出用LaTeXテンプレート ##
情報科学基礎実験用LaTeXテンプレート

ソースコード表示にjlisting.styの導入が必要

詳しくは[TeXでソースコードを埋め込む](http://qiita.com/ayihis@github/items/c779e4ab5cd7580f1f87)などを参考にすること

ただしソースコード内に日本語がない場合は導入する必要はない
