// モールス符号と画面の対応
#include <Matrix.h>
#include <Sprite.h>
// DIN, CLK, LOAD の各ピン番号
Matrix mtx = Matrix(10, 12, 11);
// モールス信号
int To = 120;
int Tu = To * 3;
int low = 1500;
// デジタル13番ピンにスピーカー接続
int SPEAKER = 13;
// スイッチ
int To_SWH = 6;
int Tu_SWH = 5;
int C_SWH = 2;

int to_now;
int tu_now;
int c_now;
int to_last;
int tu_last;
int c_last;

long lastDebounceTime = 0; 
long debounceDelay = 50; 

int morse[] = {0,0,0,0};
int count = 0;
Sprite a = Sprite
  (8,8,
   B00111100,
   B01111110,
   B11000011,
   B11000011,
   B11111111,
   B11111111,
   B11000011,
   B11000011);

Sprite b = Sprite
  (8,8,
   B11111110,
   B11000011,
   B11000011,
   B11111110,
   B11111110,
   B11000011,
   B11000011,
   B11111110);

Sprite c = Sprite
  (8,8,
   B01111110,
   B11100011,
   B11000011,
   B11000000,
   B11000000,
   B11000011,
   B11100011,
   B01111110);

Sprite d = Sprite
  (8,8,
   B11111110,
   B11000111,
   B11000011,
   B11000011,
   B11000011,
   B11000011,
   B11000111,
   B11111110);

Sprite e = Sprite
  (8,8,
   B11111111,
   B11000000,
   B11000000,
   B11111100,
   B11111100,
   B11000000,
   B11000000,
   B11111111);


Sprite f = Sprite
  (8,8,
   B11111111,
   B11000000,
   B11000000,
   B11111100,
   B11000000,
   B11000000,
   B11000000,
   B11000000);

Sprite g = Sprite
  (8,8,
   B01111110,
   B11100111,
   B11000011,
   B11000000,
   B11000000,
   B11000111,
   B11000011,
   B01111111);

Sprite h = Sprite
  (8,8,
   B11000011,
   B11000011,
   B11000011,
   B11111111,
   B11111111,
   B11000011,
   B11000011,
   B11000011);


Sprite i = Sprite
  (8,8,
   B00111100,
   B00011000,
   B00011000,
   B00011000,
   B00011000,
   B00011000,
   B00011000,
   B00111100);

Sprite j = Sprite
  (8,8,
   B00000011,
   B00000011,
   B00000011,
   B00000011,
   B00000011,
   B11000011,
   B11100111,
   B01111110);

Sprite k = Sprite
  (8,8,
   B11000111,
   B11001110,
   B11011100,
   B11111000,
   B11111000,
   B11011100,
   B11001110,
   B11000111);


Sprite l = Sprite
  (8,8,
   B11000000,
   B11000000,
   B11000000,
   B11000000,
   B11000000,
   B11000000,
   B11111111,
   B11111111);

Sprite m = Sprite
  (8,8,
   B11000011,
   B11100111,
   B11111111,
   B11111111,
   B11011011,
   B11011011,
   B11000011,
   B11000011);

Sprite n = Sprite
  (8,8,
    B11000011,
    B11100011,
    B11110011,
    B11111011,
    B11011111,
    B11001111,
    B11000111,
    B11000011);

Sprite o = Sprite
  (8,8,
   B01111110,
   B11100111,
   B11000011,
   B11000011,
   B11000011,
   B11000011,
   B11100111,
   B01111110);

Sprite p = Sprite
  (8,8,
   B11111110,
   B11000011,
   B11000011,
   B11111110,
   B11111100,
   B11000000,
   B11000000,
   B11000000);

Sprite q = Sprite
  (8,8,
   B01111110,
   B11100111,
   B11000011,
   B11000011,
   B11001011,
   B11011111,
   B11101111,
   B01111111);

Sprite r = Sprite
  (8,8,
   B11111110,
   B11000011,
   B11000011,
   B11111110,
   B11111100,
   B11001110,
   B11000111,
   B11000011);

Sprite s = Sprite
  (8,8,
   B01111110,
   B11100011,
   B11100000,
   B01111100,
   B00111110,
   B00000111,
   B11000111,
   B01111110);

Sprite t = Sprite
  (8,8,
   B11111111,
   B11111111,
   B00011000,
   B00011000,
   B00011000,
   B00011000,
   B00011000,
   B00011000);

Sprite u = Sprite
  (8,8,
   B11000011,
   B11000011,
   B11000011,
   B11000011,
   B11000011,
   B11000011,
   B11100111,
   B01111110);

Sprite v = Sprite
  (8,8,
   B10000001,
   B11000011,
   B11000011,
   B11100111,
   B11100111,
   B01100110,
   B00111100,
   B00011000);

Sprite w = Sprite
  (8,8,
   B10000001,
   B10000001,
   B11011011,
   B11011011,
   B11111111,
   B11111111,
   B11100111,
   B01000010);

Sprite x = Sprite
  (8,8,
   B11000011,
   B11000011,
   B11100111,
   B01111110,
   B00111100,
   B11100111,
   B11000011,
   B11000011);

Sprite y = Sprite
  (8,8,
   B11000011,
   B11000011,
   B11100111,
   B01111110,
   B00111100,
   B00011000,
   B00011000,
   B00011000);

Sprite z = Sprite
  (8,8,
   B11111111,
   B00000011,
   B00001100,
   B00011000,
   B00110000,
   B01100000,
   B11000000,
   B11111111);


void sound(int tone_delay){
  digitalWrite(SPEAKER, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(SPEAKER, LOW);
  delayMicroseconds(tone_delay);
}

void play(int tone, int length){
  unsigned long tone_end_time;
  tone_end_time = millis() + length;

  while(millis() < tone_end_time){
    sound(tone);
  }
}

void setup(){
// LEDマトリックスをすべて消灯
  mtx.clear(); 
  pinMode(SPEAKER, OUTPUT);
  pinMode(To_SWH, INPUT_PULLUP);
  pinMode(Tu_SWH, INPUT_PULLUP); 
  pinMode(C_SWH, INPUT_PULLUP); 

}

void loop(){
  /************************************/
  // 入力系処理 
  /************************************/
  // ト スイッチ処理
  to_now = digitalRead(To_SWH); 
  if(to_last == LOW && to_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
	morse[count] = 1;
	count++;
	play(low, To);
    }
    lastDebounceTime = now;
  }
  to_last = to_now; 
  // ツー スイッチ処理
  tu_now = digitalRead(Tu_SWH); 
  if(tu_last == LOW && tu_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
	morse[count] = 2;
	count++;
	play(low, Tu);
    }
    lastDebounceTime = now;
  }
  tu_last = tu_now; 
  // 確定 スイッチ処理
  c_now = digitalRead(C_SWH); 
  if(c_last == LOW && c_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 0 && morse[3] == 0)
	run(1);//A
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 1 && morse[3] == 1)
	run(2);//B
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 2 && morse[3] == 1)
	run(3);//C
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 1 && morse[3] == 0)
	run(4);//D
      if(morse[0] == 1 && morse[1] == 0 && morse[2] == 0 && morse[3] == 0)
	run(5);//E
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 2 && morse[3] == 1)
	run(6);//F
      if(morse[0] == 2 && morse[1] == 2 && morse[2] == 1 && morse[3] == 0)
	run(7);//G
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 1 && morse[3] == 1)
	run(8);//H
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 0 && morse[3] == 0)
	run(9);//I
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 2 && morse[3] == 2)
	run(10);//J
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 2 && morse[3] == 0)
	run(11);//K
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 1 && morse[3] == 1)
	run(12);//L
      if(morse[0] == 2 && morse[1] == 2 && morse[2] == 0 && morse[3] == 0)
	run(13);//M
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 0 && morse[3] == 0)
	run(14);//N
      if(morse[0] == 2 && morse[1] == 2 && morse[2] == 2 && morse[3] == 0)
	run(15);//O
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 2 && morse[3] == 1)
	run(16);//P
      if(morse[0] == 2 && morse[1] == 2 && morse[2] == 1 && morse[3] == 2)
	run(17);//Q
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 1 && morse[3] == 0)
	run(18);//R
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 1 && morse[3] == 0)
	run(19);//S
      if(morse[0] == 2 && morse[1] == 0 && morse[2] == 0 && morse[3] == 0)
	run(20);//T
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 2 && morse[3] == 0)
	run(21);//U
      if(morse[0] == 1 && morse[1] == 1 && morse[2] == 1 && morse[3] == 2)
	run(22);//V
      if(morse[0] == 1 && morse[1] == 2 && morse[2] == 2 && morse[3] == 0)
	run(23);//W
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 1 && morse[3] == 2)
	run(24);//X
      if(morse[0] == 2 && morse[1] == 1 && morse[2] == 2 && morse[3] == 2)
	run(25);//Y
      if(morse[0] == 2 && morse[1] == 2 && morse[2] == 1 && morse[3] == 1)
	run(26);//Z
      morse[0] = 0;
      morse[1] = 0;  
      morse[2] = 0;
      morse[3] = 0;
      count = 0;
      
    }
    lastDebounceTime = now;
  }
  c_last = c_now; 
}
void run(int letter){
  switch (letter){
  case 1:
    mtx.write(0,0,a);
    break;
  case 2:
    mtx.write(0,0,b);
    break;
  case 3:
    mtx.write(0,0,c);
    break;
  case 4:
    mtx.write(0,0,d);
    break;
  case 5:
    mtx.write(0,0,e);
    break;
  case 6:
    mtx.write(0,0,f);
    break;
  case 7:
    mtx.write(0,0,g);
    break;
  case 8:
    mtx.write(0,0,h);
    break;
  case 9:
    mtx.write(0,0,i);
    break;
  case 10:
    mtx.write(0,0,j);
    break;
  case 11:
    mtx.write(0,0,k);
    break;
  case 12:
    mtx.write(0,0,l);
    break;
  case 13:
    mtx.write(0,0,m);
    break;
  case 14:
    mtx.write(0,0,n);
    break;
  case 15:
    mtx.write(0,0,o);
    break;
  case 16:
    mtx.write(0,0,p);
    break;
  case 17:
    mtx.write(0,0,q);
    break;
  case 18:
    mtx.write(0,0,r);
    break;
  case 19:
    mtx.write(0,0,s);
    break;
  case 20:
    mtx.write(0,0,t);
    break;
  case 21:
    mtx.write(0,0,u);
    break;
  case 22:
    mtx.write(0,0,v);
    break;
  case 23:
    mtx.write(0,0,w);
    break;
  case 24:
    mtx.write(0,0,x);
    break;
  case 25:
    mtx.write(0,0,y);
    break;
  case 26:
    mtx.write(0,0,z);
    break;
  default:
    mtx.clear();
  }
  delay(1400);
  mtx.clear();
}
