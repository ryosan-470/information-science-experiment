// 実験課題2-2 ICを使用したLEDマトリックスの点灯
// Spriteライブラリを用いる
#include <Sprite.h>
#include <Matrix.h>
Matrix mtx = Matrix(10, 12, 11);
Sprite tsuku = Sprite( // スプライト 1 文字目
		      8, 7,
		      B01001000,
		      B01101110,
		      B10110100,
		      B00000000,
		      B11101100,
		      B01001100,
		      B11110110);
Sprite ba = Sprite( // スプライト 2 文字目
		   8, 7,
		   B10001000,
		   B00111110,
		   B10101010,
		   B00111100,
		   B10110100,
		   B10101000,
		   B11010110);
void setup() { mtx.clear(); }
int x = 0;
void loop() {
  mtx.write(0 - x, 0, tsuku); // スプライトの書き込み
  mtx.write(8 - x, 0, ba);
  mtx.write(16 - x, 0, tsuku);
  delay(100);
  mtx.clear(); // 画面のクリア
  if (x == 16) x = 0;
  x++;
  }