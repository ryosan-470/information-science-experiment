// 実験課題2-1 ICを使用したLEDマトリックスの点灯
#include <Matrix.h>
#include <Sprite.h>
Matrix mtx = Matrix(10, 12, 11); // DIN, CLK, LOAD の各ピン番号
void setup() {
  mtx.clear(); // LED マトリックスをすべて消灯
}
void loop() {
  for (int j = 0; j < 8; j++) {
    for (int i = 0; i < 8; i++) {
      mtx.write(i, j, HIGH); // (i,j) の位置の LED を点灯
      delay(100);
      mtx.write(i, j, LOW); // (i,j) の位置の LED を消灯
    }
  }
}