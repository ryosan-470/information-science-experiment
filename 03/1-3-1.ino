// 実験課題1-3  点滅するLEDの移動
// タクトスイッチによるLEDの動きの変化
int counter = 11;

int SWITCH = 13;
int sw_now;      // 現在のスイッチの状態
int sw_last;     // 前回のスイッチの状態 
long lastDebounceTime = 0;
long debounceDelay = 50;

int DELAY = 100;
boolean dic = true; // trueなら順方向, falseなら逆方向
void setup(){
  int i;
  for(i = 2; i < 12; i++){
    pinMode(i, OUTPUT);
  }
  pinMode(SWITCH, INPUT);
}

void loop(){
  // チャタリング防止の記述
  sw_now = digitalRead(SWITCH); 
  if(sw_last == LOW && sw_now == HIGH){
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {	    
      if(dic == false){
	dic = true;
	reverse(counter);
      }else{
	dic = false;
	forward(counter);
      }
    }
    lastDebounceTime = now;
  }
  sw_last = sw_now; 

  if(counter > 12)
    counter--;
  else if(counter < 1)
    counter++;
}
// j 番目から順方向に進む
void forward(int j){
  int i;
  for(i = j; i > 1; i--){
    digitalWrite(i, HIGH);
    delay(DELAY);
    digitalWrite(i, LOW);    
    delay(DELAY);
  }
}
// j 番目から逆方向に進む
void reverse(int j){
  int i;
  for(i = j; i < 12; i++){
    digitalWrite(i, HIGH);
    delay(DELAY);
    digitalWrite(i, LOW);    
    delay(DELAY);
  }
}
