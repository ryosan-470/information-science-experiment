// 実験課題1 点滅するLEDの移動
// 単方向
int ledPin = 11;

void setup(){
  int i;
  for(i = 2; i < 12; i++){
    pinMode(i, OUTPUT);
  }
}

void loop(){
  if(ledPin < 1){
    ledPin = 11;
  }
  digitalWrite(ledPin, HIGH);
  delay(10);
  digitalWrite(ledPin, LOW);
  ledPin--;
}
