// 実験課題1-2 点滅するLEDの移動
// 双方向
int DELAY = 100;
void setup(){
  int i;
  for(i = 2; i < 12; i++){
    pinMode(i, OUTPUT);
  }
}

void loop(){
  forward();
  reverse();
}
void forward(){
  int i;
  for(i = 11; i > 2; i--){
    digitalWrite(i, HIGH);
    delay(DELAY);
    digitalWrite(i, LOW);
    delay(DELAY);
  }
}
void reverse(){
  int i;
  for(i = 2; i < 11; i++){
    digitalWrite(i, HIGH);
    delay(DELAY);
    digitalWrite(i, LOW);    
    delay(DELAY);
  }
}
