// 応用実験 加速度センサの利用
// POV
#define DELAY 10  // 点滅間隔
#define LEDPIN 2  // LEDピンの一番若い
#define BITMAPLEN 23 // ビットマップの大きさ
unsigned char pos = 0;
unsigned char i = 0;

int analogPin = 0; // 加速度センサをアナログ0番に

int center = 500;
// 文字列データ // HELLO COINS 
int bitmap[] = {
  0b0000000000,
  0b0000000111,
  0b0000111000,
  0b0011000000,
  0b1100000000,
  0b0011000000,
  0b0000111000,
  0b0000000111,
  0b0000000000,
  0b0000000000,
  0b1100000011,
  0b1100000011,
  0b1111111111,
  0b1100000011,
  0b1100000011,
  0b0000000000,
  0b0000000000,
  0b1111111111,
  0b0000011100,
  0b0001100000,
  0b0000011100,
  0b1111111111,
  0b0000000000,
  0b0000000000,
};
void setup(){
  int pin;
  for(pin = LEDPIN; pin < LEDPIN + 10;pin++)
    pinMode(pin, OUTPUT);
  Serial.begin(9600);  
}

void loop(){
  int val;
  val = analogRead(analogPin);
  Serial.println(val-center);
  if(val - center > 0){
    for(i = 0; i < 10; i++){
      digitalWrite(LEDPIN + i, (bitmap[pos] >> i) & 0b000000001);
    }
    if(pos++ == BITMAPLEN)
      pos = 0;
    delay(DELAY);
  }
}