// 実験課題2
// POV
// とりあえず X を出力
int DELAY_TIME = 10;

void setup(){
  int i;
  for(i = 2; i < 12; i++)
    pinMode(i, OUTPUT);
}

void loop(){
  int i, j;
  for(i = 2; i < 12; i++){
    j = 13-i;
    digitalWrite(i, HIGH);
    digitalWrite(j, HIGH);
    delay(DELAY_TIME);
    digitalWrite(i, LOW);
    digitalWrite(j, LOW);
    delay(DELAY_TIME);
  }
}